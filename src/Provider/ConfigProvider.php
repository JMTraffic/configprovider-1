<?php
/**
 * Configuration Loader Provider
 */
namespace Selene\ConfigProvider\Provider;

use Pimple\Container;
use Pimple\ServiceProviderInterface;
use Silex\Application;
use Silex\Api\BootableProviderInterface;
use Selene\ConfigProvider\Driver;

class ConfigProvider implements ServiceProviderInterface, BootableProviderInterface
{

    public function register(Container $app)
    {
        if (isset($app['config.file'])) {
            $app['config.file'] = 'config.php';
        }
    }

    public function boot(Application $app)
    {
        $filedata = pathinfo($app['config.file']);

        switch ($filedata['extension']) {
        case 'json':
        case 'jsn':
            $driver = new Driver\JsonDriver($app['config.file']);
            break;
        case 'yaml':
        case 'yml':
            $driver = new Driver\YamlDriver;
            break;
        case 'php':
        $driver = new PhpDriver;
        default:

        }

        $driver->loadFile($app['config.file']);
        $driver->process($app);
    }

}
