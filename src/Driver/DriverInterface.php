<?php

namespace Selene\ConfigProvider\Driver;

use Silex\Application;

interface DriverInterface
{
    public function loadFile($filename);
    public function process(Application $app);
}
