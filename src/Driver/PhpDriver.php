<?php

namespace Selene\ConfigProvider\Driver;

use Silex\Application;
use Selene\ConfigProvider\Driver\DriverInterface;
use Selene\ConfigProvider\Exception\FileNotFound;

class PhpDriver implements DriverInterface
{
    public function loadFile($filename)
    {
    }

    public function process(Application $app)
    {
    }
}
