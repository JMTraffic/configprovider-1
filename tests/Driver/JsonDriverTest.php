<?php

namespace Selene\ConfigProvider\Driver;

use Silex\Application;
use Selene\ConfigProvider\Driver\JsonDriver;
use Selene\ConfigProvider\Exception\FileNotFound;
use PHPUnit\Framework\TestCase;

class JsonDriverTest extends TestCase
{
    public function testLoadFile()
    {
        $driver = new JsonDriver;
        $this->assertTrue($driver->loadFile('test.json'));
    }

    public function testLoadFileNotFound()
    {
        $this->expectException(FileNotFound::class);
        $driver = new JsonDriver;
        $driver->loadFile('not_found.json');
    }

    public function testProcessFile()
    {
        // Oh noes!  Why aren't I actually mocking the application?  Because of offsetSet, you dumb turds.
        // It's hard to create the mock with offsetSet and have it do what it is supposed to do.
        // So in the interest of time and sanity, I'm calling the application and using it.
        $mock = new Application;

        $driver = new JsonDriver;
        $driver->loadFile('test.json');
        $driver->process($mock);
        $this->assertTrue($mock['debug']);
    }

}
